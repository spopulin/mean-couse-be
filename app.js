const express = require('express');
const bodyParser = require('body-parser');

const Post = require('./models/post');

const app = express();

app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, DELETE, OPTIONS');
    next();
})

app.post('/api/posts', (req, res, next) => {
    const post = new Post({
        title: req.body.title,
        content: req.body.content
    })
    console.log(post)
    res.status(201).json({
        message: 'Post created successfully'
    });
});

app.get('/api/posts', (req, res, next) => {
    const posts = [
        {
            id: '1',
            title: 'First dummy post',
            content: 'This is coming from the server bro!'
        },
        {
            id: '2',
            title: 'Second dummy post',
            content: 'This is coming from the server bro!'
        }
    ];
    res.status(200).json({
        message: 'posts fetched succesfully!',
        posts: posts
    });
});

module.exports = app; 
